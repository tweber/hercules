#!/bin/bash

find . -name "Tut*" -type d -exec rm -rfv {} \;
rm -fv Tut*.
rm -fv Tut*.c
rm -fv *.out
rm -fv *.bin
